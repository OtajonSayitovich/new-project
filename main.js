let reference = [
    {
        idraqam: "154",
        ism: "Xurshid",
        familiya: "Uktamov",
        yosh: 20,
        fan: "kimyo",
        tel: +998907777777
    },

    {
        idraqam: "51",
        ism: "Hasanov",
        familiya: "Ulug'bek",
        yosh: 24,
        fan: "kimyo",
        tel: +998907777777
    },

    {
        idraqam: "76",
        ism: "Ravshanov",
        familiya: "Rahim",
        yosh: 22,
        fan: "mneomonika",
        tel: +998907777777
    },

    {
        idraqam: "124",
        ism: "Suyunov",
        familiya: "Azim",
        yosh: 19,
        fan: "biologiya",
        tel: +998907777777
    }
];

let edited=-1;

function listOfWorks() {
    document.getElementById("list").innerHTML = '';
    for (let i = 0; i < reference.length; i++) {
        document.getElementById("list").innerHTML +=
            '<tr>' +
            '<td>' + (i + 1) + '</td>' +
            '<td>' + reference[i].idraqam + '</td>' +
            '<td>' + reference[i].ism + '</td>' +
            '<td>' + reference[i].familiya + '</td>' +
            '<td>' + reference[i].yosh + '</td>' +
            '<td>' + reference[i].fan + '</td>' +
            '<td>' + reference[i].tel + '</td>' +
            // onclickda qavsni ichidagi arreyni indeksini bildiradi. agar' + i + ' ko'rinishida yozmasa string deb o'qiydi
            '<td><button onclick="editWorks(' + i + ')" type="button" class="btn btn-success btn-sm"><i class="fas fa-edit"></i></button></td>' +
            '<td><button onclick="deleteWorks(' + i + ')" type="button" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button></td>' +
            '</tr>'
    }
};

listOfWorks();

function add() {
    let idraqam = document.getElementById("idraqam").value;
    let ism = document.getElementById("ism").value;
    let familiya = document.getElementById("familiya").value;
    let yosh = document.getElementById("yosh").value;
    let fan = document.getElementById("fan").value;
    let tel = document.getElementById("tel").value;

    if (idraqam.trim().length > 0 && ism.trim().length > 0 && familiya.trim().length > 0 && yosh.trim().length > 0 &&
        tel.trim().length > 0) {
        let newworks = {
            idraqam: idraqam,
            ism: ism,
            familiya: familiya,
            yosh: yosh,
            fan: fan,
            tel: tel
        };

        if (edited>=0) {
            reference[edited]=newworks;
            edited=-1
        }

        else {
            reference.push(newworks);
        }

        listOfWorks();
        document.forms['myForm'].reset();

    }

    else alert("Ma'lumotlarni to'ldiring")

}


// indexni o'rniga hohlagan so'zni qo'yish mumkin
function deleteWorks(index) {
    reference.splice(index,1);
    listOfWorks();
};

function editWorks(index) {
    document.getElementById("idraqam").value=reference[index].idraqam;
    document.getElementById("ism").value=reference[index].ism;
    document.getElementById("familiya").value=reference[index].familiya;
    document.getElementById("yosh").value=reference[index].yosh;
    document.getElementById("fan").value=reference[index].fan;
    document.getElementById("tel").value=reference[index].tel;
    edited=index;
}